FLAGS = -felf64 -g
ASM=nasm
LD=ld -o
main: main.o lib.o dict.o
	$(LD) $@ $^

main.o: main.asm colon.inc words.inc
	$(ASM) $(FLAGS)  -o $@ $<

dict.o: dict.asm 
	$(ASM) $(FLAGS) -o $@ $<


lib.o: lib.asm 
	$(ASM) $(FLAGS) -o $@ $<

clean:
	rm *.o main
