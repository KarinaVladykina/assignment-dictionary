%define pointer 0
%macro colon 2

%%CURRENT:
dq pointer
db %1, 0

%define pointer %%CURRENT
%2:
%endmacro 
