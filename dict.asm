;В файле dict.asm создать функцию find_word. Она принимает два аргумента:
;Указатель на нуль-терминированную строку.
;Указатель на начало словаря.
;find_word пройдёт по всему словарю в поисках подходящего ключа. Если подходящее вхождение найдено, вернёт адрес начала вхождения в словарь (не значения), иначе вернёт 0.

extern string_equals
global find_word

find_word:
.find:
    add rsi, 8 
    push rdi
    push rsi
    call string_equals
    test rax, rax
    jnz .end
    mov rsi, [rsi-8] 
    cmp rsi, 0
    jne .find 
.result:
    xor rax, rax
    ret
.end:
    mov rax, rsi
    ret      



