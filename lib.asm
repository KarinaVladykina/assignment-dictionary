section .data
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_int
global parse_uint
global string_copy
global print_error


space_character: db 0xA

section .text

; Принимает код возврата и завершает текущий процесс
exit: 
	mov rax, 60
	syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
	cmp byte [rdi+rax], 0
	je .end
	inc rax
	jmp .loop
.end:
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	call string_length
	mov rsi, rdi
	mov rdi, 1
	mov rdx, rax
	mov rax, 1
	syscall 
    ret

print_error:
    call string_length 
    mov rsi, rdi 
    mov rdx, rax 
    mov rax, 1 
    mov rdi, 2  
    syscall
    ret


; Принимает код символа и выводит его в stdout
print_char:
	mov rax, 1
	push rdi
	mov rsi, rsp
	mov rdi, 1
	mov rdx, 1
	syscall
	pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, space_character
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
; rdi
print_uint:
	push r12
	mov r12, rsp
	mov rax, rdi
	mov rsi, 10
	dec rsp
.loop:
	xor rdx, rdx
		div rsi
		add rdx, '0'
		dec rsp
		mov byte[rsp], dl
		test rax, rax
		jnz .loop

		
		mov rdi, rsp
		call print_string
		mov rsp, r12
		pop r12
		ret
	


; Выводит знаковое 8-байтовое число в десятичном формате
;rdi 
print_int:
    test rdi, rdi 
    jns print_uint 
    push rdi 
    mov rdi, '-' 
    call print_char 
    pop rdi 
    neg rdi
    jmp print_uint



; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	xor rcx, rcx
.loop:
	mov al, byte[rdi+rcx]
	cmp byte[rsi+rcx], al
	jne .ret_neq
	test al, al
	jz .ret_eq
	inc rcx
	jmp .loop
.ret_eq:
	mov rax, 1
	ret
.ret_neq:
	xor rax, rax
	ret
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	push byte 0
	xor rax, rax
	xor rdi, rdi
	mov rsi, rsp
	mov rdx, 1
	syscall
	test rax, rax
	js .error
	pop rax
	ret
.error:
	xor rax, rax
	ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	push r12
	push r14
	mov r12, rdi
.skip_whitespace:
	call read_char
	test rax, rax
	jz .error
	cmp rax, 32
	je .skip_whitespace
	cmp rax, 10
	je .skip_whitespace
	cmp rax, 9
	je .skip_whitespace
	xor r14, r14
.loop:
	cmp r14, rsi
	jge .error
	mov [r12+r14], al
	inc r14
	call read_char
	test rax, rax
	jz .word_end
	cmp rax, 32
	je .word_end
	cmp rax, 10
	je .word_end
	cmp rax, 9
	je .word_end
	jmp .loop
.word_end:
	cmp r14, rsi
	jge .error
	mov byte [r12+r14], 0
	mov rax, r12
	mov rdx, r14
	jmp .ret
.error:
	xor rax, rax
	xor rdx, rdx
.ret:
	pop r14
	pop r12
	ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	push r13
	xor rax, rax
	xor rdx, rdx
	xor r13, r13
.loop:
	mov r13b, byte[rdi+rdx]
	sub r13b, '0'
	js .exit
	cmp r13b, 9
	jg .exit
	imul rax, 0xA
	add rax, r13
	inc rdx
	jmp .loop
.exit:
	pop r13
	ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	cmp byte [rdi], '+'
	je .parse_pos
	cmp byte [rdi], '-'
	je .parse_neg
	jmp parse_uint
.parse_pos:
	inc rdi
	call parse_uint
	inc rdx
	ret
.parse_neg:
	inc rdi
	call parse_uint
	neg rax
	inc rdx
	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor rax, rax
	test rdx, rdx
	jz .exit
.loop:
	mov cl, byte[rdi+rax]
	mov byte[rsi+rax], cl
	test cl, cl
	jz .exit
	inc rax
	cmp rax, rdx
	jle .loop
	xor rax, rax
.exit:
	ret
