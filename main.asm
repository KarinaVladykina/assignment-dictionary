%include "colon.inc"
%include "words.inc"
%include "lib.inc"
%define buffer_size 256
global _start
extern find_word

section .rodata
starting_message: db "Введите ключ для поиска в словаре: ", 10, 0
not_found: db "Ключ отсутствует в словаре", 10, 0
error_length: db "Слишком длинный ключ, он должен быть меньше, чем 256 символов", 10, 0

section .text
_start:
    mov rdi, starting_message
	call print_string
	mov rdi, rsp 
	mov rsi, buffer_size 
	call read_word
	cmp rax, 0 ; проверяем, подходит ли строка по размеру 
	je .show_error_length
	mov rsi , pointer
	mov rdi, rax
	call find_word
	add rsp, buffer_size ; возвращаем стек в начальное состояние
	cmp rax, 0 ; проверяем , было ли найдено слово
	je .no_word	
	add rax, 8  
	push rax 
	mov rdi, [rsp]  ;Кладем указатель на ключ 
	call string_length
	pop rdi
	add rdi, rax 
	inc rdi ;Увеличиваем длину 
	call print_string
	call exit
	
.show_error_length:
	add rsp, buffer_size
	mov rdi, error_length
	call print_error
	call exit

.no_word:
	mov rdi, not_found
	call print_error
	call exit






