section .rodata
colon "bus", bus
db "a public transport road vehicle designed to carry significantly more passengers than the average cars or vans.", 0

colon "bicycle", bicycle
db "a human-powered or motor-powered, pedal-driven, single-track vehicle, having two wheels attached to a frame, one behind the other", 0

colon "car", car
db "a wheeled motor vehicle used for transportation", 0
